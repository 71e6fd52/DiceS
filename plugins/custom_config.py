import sys
import os
import datetime
import platform

sys.path.append('..')

import utils
from master_config import *

# 刷屏上限
MESSAGE_LIMIT_NUM = 12
# 刷屏阈值
MESSAGE_LIMIT_TIME = datetime.timedelta(seconds=6)
# 聊天命令回复间隔
CHAT_LIMIT_TIME = datetime.timedelta(seconds=10)
# 交互命令上限
IA_LIMIT_NUM = 3
# 交互命令有效期
IA_EXPIRE_TIME = datetime.timedelta(seconds=60)
# 查询最多显示的条目
QUERY_SHOW_LIMIT = 50

LOCAL_DATA_PATH = os.path.join(os.path.dirname(__file__), 'data')
LOCAL_NICKNAME_PATH = os.path.join(LOCAL_DATA_PATH, 'nick_name.json')
LOCAL_INITINFO_PATH = os.path.join(LOCAL_DATA_PATH, 'init_info.json')
LOCAL_PCSTATE_PATH = os.path.join(LOCAL_DATA_PATH, 'pc_state.json')
LOCAL_GROUPINFO_PATH = os.path.join(LOCAL_DATA_PATH, 'group_info.json')
LOCAL_USERINFO_PATH = os.path.join(LOCAL_DATA_PATH, 'user_info.json')
LOCAL_TEAMINFO_PATH = os.path.join(LOCAL_DATA_PATH, 'team_info.json')
LOCAL_DAILYINFO_PATH = os.path.join(LOCAL_DATA_PATH, 'daily_info.json')
LOCAL_MEMBERINFO_PATH = os.path.join(LOCAL_DATA_PATH, 'group_member_info.json')
LOCAL_MASTERINFO_PATH = os.path.join(LOCAL_DATA_PATH, 'master_info.json')

LOCAL_CUSTOM_DATA_PATH = os.path.join(os.path.dirname(__file__), 'custom_data')
LOCAL_QUERYINFO_DIR_PATH = os.path.join(LOCAL_CUSTOM_DATA_PATH, 'query_info')
LOCAL_DECKINFO_DIR_PATH = os.path.join(LOCAL_CUSTOM_DATA_PATH, 'deck_info')
LOCAL_NAMEINFO_PATH = os.path.join(LOCAL_CUSTOM_DATA_PATH, 'name.json')
LOCAL_QUESINFO_PATH = os.path.join(LOCAL_CUSTOM_DATA_PATH, 'question.json')
LOCAL_EMOTINFO_DIR_PATH = os.path.join(LOCAL_CUSTOM_DATA_PATH, 'emotion_img')

LOCAL_EMOTIMG_DIR_PATH = os.path.join(LOCAL_CUSTOM_DATA_PATH, 'emotion_img')

LOCAL_LOG_DIR_PATH = os.path.join(LOCAL_DATA_PATH, 'log')

ALL_LOCAL_DATA_PATH = [
    LOCAL_NICKNAME_PATH, LOCAL_INITINFO_PATH, LOCAL_PCSTATE_PATH,
    LOCAL_GROUPINFO_PATH, LOCAL_USERINFO_PATH, LOCAL_TEAMINFO_PATH,
    LOCAL_DAILYINFO_PATH, LOCAL_MEMBERINFO_PATH, LOCAL_MASTERINFO_PATH
]

ALL_LOCAL_DIR_PATH = [
    LOCAL_DATA_PATH, LOCAL_CUSTOM_DATA_PATH, LOCAL_QUERYINFO_DIR_PATH,
    LOCAL_DECKINFO_DIR_PATH, LOCAL_DECKINFO_DIR_PATH, LOCAL_EMOTIMG_DIR_PATH,
    LOCAL_LOG_DIR_PATH
]

for dirPath in ALL_LOCAL_DIR_PATH:
    if os.path.exists(dirPath) == False:
        os.makedirs(dirPath)
        print("Make dir: " + dirPath)

for path in ALL_LOCAL_DATA_PATH:
    if os.path.exists(path) == False:
        utils.UpdateJson({}, path)
        print("Create file: " + path)
