import re


def InsertEmotion(inputStr, emotionDict):
    def replaceByEmotion(matched):
        keyW = matched.group()[1:-1]
        try:
            keyW = f'[CQ:image,file=file:///{emotionDict[keyW]}]'
        except KeyError:
            keyW = f'#{keyW}'
        return keyW

    if emotionDict:
        return re.sub('\$.+?\$', replaceByEmotion, inputStr)
    else:
        return inputStr


BOT_NAME = '休比'

GIFT_LIST = [
    '西洋棋', '虚空第零加护', '真典·杀星者', '髓爆', '对未知用战斗演算', '心', '星杯', '小吉炖蘑菇', '「通行管制」',
    '代码1673B753E1F255脚本E', '阿尔特休', '十条盟约', '特图', '望远镜', '阿邦特·赫伊姆'
]

BOT_ON_STR = '[典开]——游戏002「骰子」——'
BOT_OFF_STR = '[待机]'

ROLL_FEED_STR = '{reason}{nickName} 掷出了 {resultStr}'  # 普通的掷骰指令
ROLL_REASON_STR = '由于{reason}……'  # reason部分的格式
N20_FEED_STR = '，N20……checkmate'  # 单次掷骰大成功提示
N01_FEED_STR = '，N1'  # 单次掷骰大失败提示
N20_FEED_MULT_STR = '{succTimes}次……N20'  # 多次掷骰大成功提示
N01_FEED_MULT_STR = '{failTimes}次……N1'  # 多次掷骰大失败提示

HROLL_FEED_STR = '{nickName}……暗骰'  # 使用暗骰指令后在群里发送的提示
HROLL_RES_STR = '暗骰结果:{finalResult}'  # 使用暗骰指令后私聊发送的提示

JRRP_FEED_STR = '{nickName}的胜率是……{value}幺科托'  # JRRP指令回复
JRRP_GOOD_STR = '，但是……游戏还没结束'  # 好运提示
JRRP_BAD_STR = '，你的每次检定……成功机率……都不满1%……建议……使用{gift}……'  # 厄运提示
JRRP_REPEAT_STR = '[读取]{nickName}的胜率是{value}幺科托'

NN_FEED_STR = '设定……你的名字为……{nickName}'  # nn指令回复
NN_RESET_STR = '恢复个体识别号码'

INIT_MISS_STR = '先攻列表……不存在'
INIT_CLEAR_STR = '先攻列表……删除完成'
INIT_CLEAR_FAIL_STR = '先攻列表……不存在'
INIT_REMOVE_STR = '已将{name}……从先攻列表中删除'
INIT_REMOVE_FAIL_STR = '先攻列表中……不存在……{name}'
INIT_REMOVE_MULT_STR = '先攻列表中……多个相同的名字: {val}'
INIT_WARN_STR = '警告……先攻列表……已过时'

DISMISS_FEED_STR = '解除连结'  # 使用dismiss指令退群后发送
NICKNAME_LEN_LIMIT_STR = '[错误]名字超过资料库允许长度'  # 昵称超过20个字符后发送的提示
GROUP_COMMAND_ONLY_STR = '[错误]无法在私聊语境理解该指令'  # 在私聊中使用群聊功能时发送的提示
HP_CLEAR_STR = '已清除{nickName}的生命值……'  # 删除生命值后的提示
PC_CLEAR_STR = '已删除{nickName}的角色卡……'  # 删除角色卡后的提示
CHECK_TIME_LIMIT_STR = '重复的次数……需要在1-9之间'  # 检定次数超出范围后的提示
TEAM_NEED_STR = '必须……先加入队伍'  # 需要先加入队伍的提示
TEAM_INFO_FEED_STR = '已将……队伍的完整信息^私聊发送'  # 查看队伍信息后的提示
SPELL_SLOT_ADJ_INVALID_STR = '{val}是……无效的法术位调整值……合法范围:[-9, +9]'  # 无效法术位调整提示
SEND_LEN_LIMIT_STR = '请不要骚扰「意志者」 (信息长度限制为10~100)'  # 给Master发送的信息超过范围后的提示
SEND_FEED_STR = '信息……转发给「意志者」……完成'  # 给Master发送信息后的提示
WELCOME_FEED_STR = '设置完成……入群欢迎词:'  # 设置入群关键词
WELCOME_CLEAR_STR = '关闭入群欢迎'  # 关闭入群关键词
NAME_FORMAT_STR = '[错误]输入的格式无法识别'  # 生成随机姓名指令格式不正确
NAME_LIMIT_STR = '生成的名字个数必须在1-10之间'  # 生成随机姓名指令数量不正确
DND_FEED_STR = '{nickName}的初始属性: {reason}\n{result}'  # 生成属性的回复
ERROR2MASTER_STR = '[错误][矛盾][违反][错误][矛盾]请求与帆楼的连结'  # 遇到错误时的反馈
MASTER_LIMIT_STR = '只有「意志者」……可以使用此指令'
SAVE_FEED_STR = '成功……将所有资料……保存到本地'
INFO_MISS_STR = '{val}资料库加载……失败了'

NOTE_FEED_STR = '已记录……索引是……{index}'
NOTE_NUM_LIMIT_STR = '资料库限额……一个群内……最多20条'
NOTE_LEN_LIMIT_STR = '资料库限额……不允许……超过三百个字'
NOTE_TOTAL_LIMIT_STR = '资料库限额……一个群内……最多保存两千字'
NOTE_CLEAR_ALL_STR = '成功删除……所有笔记'
NOTE_CLEAR_STR = '成功删除……索引为{index}的笔记~'
NOTE_MULT_INDEX_STR = '可能的笔记索引:{resList}'
NOTE_MISS_STR = '无法找到……相应的笔记索引'

QUERY_NOTICE_STR = '资料库中共有{num}个条目……可查询内容……请输入 .help查询'
QUERY_KEY_LIMIT_STR = '指定的关键词……太多'
QUERY_MULT_SHORT_STR = '找到……多个匹配的条目: {info}\n回复序号……可直接查询对应内容'
QUERY_MULT_LONG_STR = '找到……多个匹配的条目: {info}等，共{num}个条目\n回复序号……可直接查询对应内容'
QUERY_MISS_STR = '资料库中……不存在'
QUERY_FEED_COMP_STR = '要找的……是{key}吗?\n{result}'
INDEX_MISS_STR = '资料库中……不存在关键字{keywordList}的词条'

DRAW_NOTICE_STR = '资料库中共有{num}个牌堆，分别是{val}'
DRAW_NUM_LIMIT_STR = '抽取的数量……必须为1-10之间的整数'
DRAW_FEED_STR = '从{targetStr}中……抽取的结果: \n{result}'
DRAW_MULT_SHORT_STR = '找到多个匹配的牌堆: {info}'
DRAW_MULT_LONG_STR = '找到多个匹配的牌堆: {info}等，共{num}个牌堆'

EXAM_LIST_STR = '当前可用的题库是：{val}'  # 题库列表
EXAM_MULT_STR = '想找的……是哪一个题库呢?\n{possKey}'  # 找到多个题库
EXAM_MISS_STR = '{key}……不是可用的题库!'  # 找不到题库
EXAM_EXPIRE_STR = '时间到达限制……'
EXAM_TRUE_STR = '回答正确……'
EXAM_FALSE_STR = '回答错误……答案是{answer}'

FUNC_ON_STR = '{funcName}功能启用……'
FUNC_OFF_STR = '{funcName}功能禁用……'
FUNC_BAN_NOTICE = '这个功能……已禁用……请使用群管理指令启用……'

LOG_START_STR = '{file}……开始记录'
LOG_EXISTS_STR = '已经在记录{currect}了……'
LOG_STOP_STR = '{file}……停止记录'
LOG_NO_STOP_STR = '没有在记录……'
LOG_NEED_NAME_STR = '日志……需要名字'
LOG_NOT_EXISTS_STR = '没有这个日志……'
LOG_DEL_STR = '{file}……删除完成'
